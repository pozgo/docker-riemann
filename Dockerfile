FROM million12/centos-supervisor
MAINTAINER Marcin Ryzycki marcin@m12.io, Przemyslaw Ozgo linux@ozgo.info

RUN \
    yum update -y && \
    yum install -y tar bzip2 java-1.7.0-openjdk ruby-devel make gcc rubygem-nokogiri && \
    curl https://aphyr.com/riemann/riemann-0.2.8.tar.bz2 -o /tmp/rieman.tar.bz2 && \
    mkdir /opt/riemann && \
    tar xvfj /tmp/rieman.tar.bz2 -C /opt/riemann --strip-components=1 && \
    rm -f /tmp/rieman.tar.bz2 && \
    gem install riemann-client riemann-tools riemann-dash && \
    yum remove -y make gcc tar bzip2 && \
    yum clean all

COPY container-files /

EXPOSE 4567
